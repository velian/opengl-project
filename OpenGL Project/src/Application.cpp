#include "Application.h"
#include "GameStateManager.h"
#include <iostream>
#include <conio.h>
#include "Gizmos/Gizmos.h"
#include <gl_core_4_4.h>
#include <GLFW/glfw3.h>

Application::Application()
{
	Initialize(800, 600);
}

Application::Application(int _screenWidth, int _screenHeight, char* _screenTitle)
{
	Initialize(_screenWidth, _screenHeight, _screenTitle);
}

int Application::Initialize(int _screenWidth, int _screenHeight, char* _screenTitle)
{
	m_running = true;
	m_deltaTime = 0.f;
	m_previousTime = 0.f;
	m_currentTime = 0.f;	

	m_clearColour = glm::vec4(0.5, 0.5, 0.5, 1);

	if (glfwInit() == false)
	{
		std::cout << "GLFW failed to initialize!";
		return -1;
	}

	m_window = glfwCreateWindow(_screenWidth, _screenHeight, _screenTitle, nullptr, nullptr);

	if (m_window == nullptr)
	{
		glfwTerminate();
		return -2;
	}

	glfwMakeContextCurrent(m_window);

	if (ogl_LoadFunctions() == ogl_LOAD_FAILED)
	{ 
		glfwDestroyWindow(m_window);
		glfwTerminate();
		return -3;
	}

	if (!InitializeTweakBar(_screenWidth, _screenHeight))
	{
		printf("Tweak bar failed to load!");
		return -4;
	}

	m_gameStateManager = new GameStateManager();

	return 1;
}

int Application::InitializeTweakBar(unsigned int _screenWidth, unsigned int _screenHeight)
{
	//Initialize Tweak Bar
	if (!TwInit(TW_OPENGL_CORE, nullptr))
	{
		return -1;
	}

	if (!TwWindowSize(_screenWidth, _screenHeight))
	{
		return -2;
	}

	glfwSetMouseButtonCallback(m_window, TweakBarFunctions::OnMouseButton);
	glfwSetCursorPosCallback(m_window, TweakBarFunctions::OnMousePosition);
	glfwSetScrollCallback(m_window, TweakBarFunctions::OnMouseScroll);
	glfwSetKeyCallback(m_window, TweakBarFunctions::OnKey);
	glfwSetCharCallback(m_window, TweakBarFunctions::OnChar);
	glfwSetWindowSizeCallback(m_window, TweakBarFunctions::OnWindowResize);

	return 1;
}

void Application::Run()
{
	
	glEnable(GL_DEPTH_TEST);
	//glEnable(GL_FRONT_AND_BACK);
	
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);	

	m_gameStateManager->SetContext(m_window);

	while (glfwWindowShouldClose(m_window) == false && m_running == true)
	{		
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	
		glClearColor(m_clearColour.x, m_clearColour.y, m_clearColour.z, m_clearColour.w);

		UpdateDelta();

		m_gameStateManager->Update(m_deltaTime);
		m_gameStateManager->Draw();

		TwDraw();

		glfwSwapBuffers(m_window);
		glfwPollEvents();

		if (glfwGetKey(m_window, GLFW_KEY_ESCAPE))
		{
			m_running = false;
		}	
	}
}

void Application::UpdateDelta()
{
	m_currentTime = (float)glfwGetTime();

	m_deltaTime = m_currentTime - m_previousTime;
	m_previousTime = m_currentTime;
}

void Application::Shutdown()
{
	TwDeleteAllBars();
	TwTerminate();
	
	
	glfwDestroyWindow(m_window);
	glfwTerminate();
}