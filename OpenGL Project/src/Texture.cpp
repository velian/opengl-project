#include "Texture.h"
#include "Camera.h"
#include "Shader.h"
#include <stb_image.h>

Texture::Texture()
{
	m_shader = new Shader();
}

void Texture::Initialize(std::string _filePath, DRAW_TYPE _drawType)
{
	m_data = stbi_load(_filePath.c_str(), &m_imageWidth, &m_imageHeight, &m_imageFormat, STBI_default);

	m_currentPath = _filePath;

	glGenTextures(1, &m_id);
	glBindTexture(GL_TEXTURE_2D, m_id);

	TEX_TYPE textureType;

	if (_filePath.substr(_filePath.find_last_of(".") + 1) == "jpg")
		textureType = JPG;

	else if (_filePath.substr(_filePath.find_last_of(".") + 1) == "png")
		textureType = PNG;

	else if (_filePath.substr(_filePath.find_last_of(".") + 1) == "tga")
		textureType = TGA;

	else
	{
		printf("Error loading texture file : %s", _filePath);
		return;
	}

	switch (textureType)
	{
		case(JPG) : glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB,  m_imageWidth, m_imageHeight, 0, GL_RGB,  GL_UNSIGNED_BYTE, m_data); break;
		case(PNG) : glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_imageWidth, m_imageHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, m_data); break;
		case(TGA) : glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB,  m_imageWidth, m_imageHeight, 0, GL_RGB,  GL_UNSIGNED_BYTE, m_data); break;
	}
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, _drawType == 0 ? GL_NEAREST : GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, _drawType == 0 ? GL_NEAREST : GL_LINEAR);

	stbi_image_free(m_data);
}

void Texture::OpenDialogue(DRAW_TYPE _drawType)
{
	OPENFILENAME dialog;

	char szFileName[MAX_PATH] = "";

	ZeroMemory(&dialog, sizeof(dialog));

	dialog.lStructSize = sizeof(dialog);

	dialog.lpstrFilter = L"PNG File (*.png)\0*.png\0JPG File (*jpg)\0*.jpg\0TGA File (*.tga)\0*.tga";
		
	dialog.lpstrFile = (LPWSTR)szFileName;
	dialog.nMaxFile = MAX_PATH;
	dialog.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST;
	//ofn.lpstrDefExt = L"txt";

	std::string texturePath;

	if (GetOpenFileName(&dialog))
	{
		texturePath = CW2A(dialog.lpstrFile);

		printf("Loaded Texture From File: %s", texturePath.c_str());
	}

	Initialize((char*)texturePath.c_str(), _drawType);
}

void Texture::Update(float _deltaTime)
{

}

void Texture::Draw(FreeCamera* _camera)
{
	//m_shader->Draw(_camera, this);
}