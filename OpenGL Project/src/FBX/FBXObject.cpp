#include "FBXObject.h"
#include "Light.h"
#include "ObjectData.h"
#include "GameState.h"
#include "AntTweakBar.h"
#include <gl_core_4_4.h>
#include <GLFW/glfw3.h>

FBXObject::FBXObject()
{
	Initialize();
}

FBXObject::FBXObject(GameState* _gameState)
{
	Initialize(_gameState);
}

void FBXObject::Initialize()
{
	m_file = new FBXFile();
	m_shader = new Shader();

	m_shader->Initialize();

	m_timer = 0;

}

void FBXObject::Initialize(GameState* _gameState)
{	
	m_light = new Light();
	InitializeTweak(_gameState);

	Initialize();	
}

void FBXObject::InitializeTweak(GameState* _gameState)
{
	m_light->m_specular = 50.f;
	m_light->m_ambientLight = 0.1f;
	m_light->m_lightDirection = glm::vec3(-0.5, -0.5, -0.5);
	m_light->m_lightColour = glm::vec3(1, 1, 1);

	TwAddVarRW(_gameState->GetTweakBar(), "Light Direction", TW_TYPE_DIR3F, &m_light->m_lightDirection, "group='Lighting'");
	TwAddVarRW(_gameState->GetTweakBar(), "Light Colour", TW_TYPE_COLOR3F, &m_light->m_lightColour, "group='Lighting'");
	TwAddVarRW(_gameState->GetTweakBar(), "Specular", TW_TYPE_FLOAT, &m_light->m_specular, "group='Lighting'");
	TwAddVarRW(_gameState->GetTweakBar(), "Ambient Light", TW_TYPE_FLOAT, &m_light->m_ambientLight, "group='Lighting'");
}

void FBXObject::Create(const char* a_filename, FBXFile::UNIT_SCALE a_scale, bool a_loadTextures, bool a_loadAnimations, bool a_flipTextureY)
{
	m_file->load(a_filename, a_scale, a_loadTextures, a_loadAnimations, a_flipTextureY);

	if (m_file == NULL)
	{
		printf("Failed to load FBX Object : %s", a_filename);
		return;
	}

	m_shader->LoadProgram(nullptr, "./shader/fbxMain.vs", "./shader/fbxMain.fs");

	for (unsigned int i = 0; i < m_file->getMeshCount(); ++i)
	{
		FBXMeshNode* pMesh = m_file->getMeshByIndex(i);

		ShaderData* meshData = new ShaderData();

		glGenVertexArrays(1, &(meshData->m_VAO));
		glGenBuffers(1, &(meshData->m_VBO));
		glGenBuffers(1, &(meshData->m_IBO));

		glBindVertexArray(meshData->m_VAO);

		glBindBuffer(GL_ARRAY_BUFFER, meshData->m_VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(FBXVertex)* pMesh->m_vertices.size(), pMesh->m_vertices.data(), GL_STATIC_DRAW);

		glEnableVertexAttribArray(0); //Position
		glEnableVertexAttribArray(1); //Indices
		glEnableVertexAttribArray(2); //Weights
		glEnableVertexAttribArray(3); //Normal
		glEnableVertexAttribArray(4); //Texcoord

		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), ((char*)0) + FBXVertex::PositionOffset);
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), ((char*)0) + FBXVertex::IndicesOffset);
		glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), ((char*)0) + FBXVertex::WeightsOffset);
		glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), ((char*)0) + FBXVertex::NormalOffset);
		glVertexAttribPointer(4, 2, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), ((char*)0) + FBXVertex::TexCoord1Offset);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, meshData->m_IBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int)* pMesh->m_indices.size(), pMesh->m_indices.data(), GL_STATIC_DRAW);

		glBindVertexArray(0);

		pMesh->m_userData = (void*)meshData;
	}
	
}

void FBXObject::Update(float _deltaTime)
{
	if (m_file->getSkeletonCount() <= 0)
	{
		return;
	}

	// grab the skeleton and animation we want to use
	FBXSkeleton* skeleton = m_file->getSkeletonByIndex(0);
	FBXAnimation* animation = m_file->getAnimationByIndex(0);

	m_timer += _deltaTime;

	// evaluate the animation to update bones
	skeleton->evaluate(animation, m_timer);

	for (unsigned int bone_index = 0; bone_index < skeleton->m_boneCount; ++bone_index)
	{
		skeleton->m_nodes[bone_index]->updateGlobalTransform();
	}
}

void FBXObject::Draw(FreeCamera* _camera)
{
	m_shader->Draw(_camera, this);
}