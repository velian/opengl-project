#include "./OBJ/Object.h"
#include "./OBJ/ObjectLoader.h"
#include "./OBJ/ObjectData.h"
#include "Shader.h"
#include "Camera.h"
#include "Texture.h"
#include "GameState\GameState.h"
#include "Light.h"

Object::Object()
{

}

Object::Object(GameState* _gameState, char* _objPath)
{
	Initialize(_gameState, _objPath);
}

Object::Object(GameState* _gameState, char* _objPath, char* _texPath)
{
	Initialize(_gameState, _objPath, _texPath);
}

Object::Object(GameState* _gameState, char* _objPath, char* _texPath, char* _glowPath)
{
	Initialize(_gameState, _objPath, _texPath, _glowPath);
}

void Object::Initialize(GameState* _gameState, char* _objPath)
{
	m_light = new Light();

	m_data = ObjectLoader::LoadObject(_objPath);
	m_shader = new Shader(_gameState);	
	m_data->tweakBar = InitializeTweak(_gameState);

	m_data->m_objectShapeSize = m_data->shapes.size();
}

void Object::Initialize(GameState* _gameState, char* _objPath, char* _texPath)
{
	m_texture = new Texture();
	m_texture->Initialize(_texPath);

	Initialize(_gameState, _objPath);
}

void Object::Initialize(GameState* _gameState, char* _objPath, char* _texPath, char* _normalPath)
{
	m_textureOverlay = new Texture();
	m_textureOverlay->Initialize(_normalPath);

	Initialize(_gameState, _objPath, _texPath);
}

TwBar* Object::InitializeTweak(GameState* _gameState)
{
	ObjectData* objectData = new ObjectData();

	m_light->m_specular = 50.f;
	m_light->m_ambientLight = 0.1f;
	m_light->m_lightDirection = glm::vec3(-0.5, -0.5, -0.5);
	m_light->m_lightColour = glm::vec3(1, 1, 1);

	objectData->tweakBar = _gameState->GetTweakBar();
	TwAddVarRW(objectData->tweakBar, "Light Direction", TW_TYPE_DIR3F, &m_light->m_lightDirection, "group='Lighting'");
	TwAddVarRW(objectData->tweakBar, "Light Colour", TW_TYPE_COLOR3F, &m_light->m_lightColour, "group='Lighting'");
	TwAddVarRW(objectData->tweakBar, "Specular", TW_TYPE_FLOAT, &m_light->m_specular, "group='Lighting'");
	TwAddVarRW(objectData->tweakBar, "Ambient Light", TW_TYPE_FLOAT, &m_light->m_ambientLight, "group='Lighting'");

	return objectData->tweakBar;
}

void Object::Create()
{	
	m_shader->LoadProgram(nullptr, "./shader/objMain.vs", "./shader/objMain.fs");

	for (unsigned int i = 0; i < m_data->shapes.size(); ++i)
	{
		glGenVertexArrays(1, &m_shaderData[i].m_VAO);
		glGenBuffers(1, &m_shaderData[i].m_VBO);
		glGenBuffers(1, &m_shaderData[i].m_IBO);
		glBindVertexArray(m_shaderData[i].m_VAO);

		unsigned int floatCount = m_data->shapes[i].mesh.positions.size();
		floatCount += m_data->shapes[i].mesh.normals.size();
		floatCount += m_data->shapes[i].mesh.texcoords.size();

		std::vector<float> vertexData;
		vertexData.reserve(floatCount);

		vertexData.insert(vertexData.end(),
			m_data->shapes[i].mesh.positions.begin(),
			m_data->shapes[i].mesh.positions.end());

		vertexData.insert(vertexData.end(),
			m_data->shapes[i].mesh.normals.begin(),
			m_data->shapes[i].mesh.normals.end());

		vertexData.insert(vertexData.end(),
			m_data->shapes[i].mesh.texcoords.begin(),
			m_data->shapes[i].mesh.texcoords.end());

		GetData()[i].m_indexCount = m_data->shapes[i].mesh.indices.size();

		glBindBuffer(GL_ARRAY_BUFFER, m_shaderData[i].m_VBO);
		glBufferData(GL_ARRAY_BUFFER, 
			vertexData.size() * sizeof(float),
			vertexData.data(), GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_shaderData[i].m_IBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_data->shapes[i].mesh.indices.size() * sizeof(unsigned int),
			m_data->shapes[i].mesh.indices.data(), GL_STATIC_DRAW);

		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_TRUE, 0, (void*)(sizeof(float)*m_data->shapes[i].mesh.positions.size()));
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, (void*)(sizeof(float)*(m_data->shapes[i].mesh.positions.size() + (m_data->shapes[i].mesh.normals.size()))));
		
		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}
}

void Object::Update()
{

}

void Object::Draw(FreeCamera* _camera)
{
	m_shader->Draw(_camera, this);
}