#include "Light.h"

Light::Light(float _ambient, float _specular, glm::vec3 _lightColour, glm::vec3 _lightDirection)
{
	m_ambientLight = _ambient;
	m_specular = _specular;
	m_lightColour = _lightColour;
	m_lightDirection = _lightDirection;
}