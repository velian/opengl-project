#include "Icon.h"
#include "Texture.h"
#include "Camera.h"

Icon::Icon(char* _filePath, DRAW_TYPE _texType)
{
	Initialize(_filePath, _texType);
}

void Icon::Initialize(char* _filePath, DRAW_TYPE _texType)
{
	m_position = glm::vec3(0, 0, 0);

	m_shader = new Shader();
	m_shaderData = new ShaderData();
	m_texture = new Texture();
	m_iconData = new IconData[4];

	m_texture->Initialize(_filePath, _texType);

	Create();
}

void Icon::Create()
{
	m_shader->LoadProgram(nullptr, "./shader/iconMain.vs", "./shader/iconMain.fs");
	
	m_iconData[0] = { glm::vec4(-2.5, 0, 2.5, 1), glm::vec2(1, 0) }; 
	m_iconData[1] = { glm::vec4(2.5, 0, 2.5, 1), glm::vec2(0, 0) }; 
	m_iconData[2] = { glm::vec4(2.5, 0, -2.5, 1), glm::vec2(0, 1) }; 
	m_iconData[3] = { glm::vec4(-2.5, 0, -2.5, 1), glm::vec2(1, 1) }; 
	
	unsigned int indexData[] = {
		0, 1, 2,
		0, 2, 3,
	};
	
	glGenVertexArrays(1, &(m_shaderData->m_VAO));
	glBindVertexArray(m_shaderData->m_VAO);
	
	glGenBuffers(1, &(m_shaderData->m_VBO));
	glGenBuffers(1, &(m_shaderData->m_IBO));
	
	glBindBuffer(GL_ARRAY_BUFFER, m_shaderData->m_VBO);
	glBufferData(GL_ARRAY_BUFFER, 4 * sizeof(IconData), m_iconData, GL_DYNAMIC_DRAW);
	
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_shaderData->m_IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * sizeof(unsigned int), indexData, GL_STATIC_DRAW);
	
	glEnableVertexAttribArray(0); //Position
	glEnableVertexAttribArray(1); //Texcoord
	
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(IconData), 0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(IconData), ((char*)0) + 16);
	
	glBindVertexArray(0);
}

void Icon::Update(FreeCamera* _camera)
{
	m_iconData[0].position = glm::vec4(2.5, 2.5, 0, 1);
	m_iconData[1].position = glm::vec4(-2.5, 2.5, 0, 1);
	m_iconData[2].position = glm::vec4(-2.5, -2.5, 0, 1);
	m_iconData[3].position = glm::vec4(2.5, -2.5, 0, 1);
	
	glm::vec3 zAxis = glm::normalize(glm::vec3(_camera->GetTransform()[3]) - m_position);
	glm::vec3 xAxis = glm::cross(glm::vec3(_camera->GetTransform()[1]), zAxis);
	glm::vec3 yAxis = cross(zAxis, xAxis);
	glm::mat4 billboard(glm::vec4(xAxis, 0),
		glm::vec4(yAxis, 0),
		glm::vec4(zAxis, 0),
		glm::vec4(0, 0, 0, 1));
	
	m_iconData[0].position = billboard * m_iconData[0].position + glm::vec4(m_position, 0);
	m_iconData[1].position = billboard * m_iconData[1].position + glm::vec4(m_position, 0);
	m_iconData[2].position = billboard * m_iconData[2].position + glm::vec4(m_position, 0);
	m_iconData[3].position = billboard * m_iconData[3].position + glm::vec4(m_position, 0);
}

void Icon::Draw(FreeCamera* _camera)
{
	glUseProgram(m_shader->m_programID);

	unsigned int location = glGetUniformLocation(m_shader->m_programID, "ProjectionView");
	glUniformMatrix4fv(location, 1, GL_FALSE, (float*)&_camera->GetProjectionView()[0][0]);

	if (GetTexture() != nullptr)
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, GetTexture()->GetID());

		location = glGetUniformLocation(m_shader->m_programID, "diffuse");
		glUniform1i(location, 0);
	}	

	glBindBuffer(GL_ARRAY_BUFFER, m_shaderData->m_VBO);
	glBufferSubData(GL_ARRAY_BUFFER, 0, 4 * sizeof(IconData), m_iconData);

	//Draw the icon
	glBindVertexArray(m_shaderData->m_VAO);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

	glUseProgram(0);
}