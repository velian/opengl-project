#include "ParticleEmitter.h"
#include "Particle.h"
#include "Shader.h"
#include "Camera.h"
#include <gl_core_4_4.h>
#include <GLFW/glfw3.h>

ParticleEmitter::ParticleEmitter(Particle* _particles, vec3 _position,
								 vec4 _startColour, vec4 _endColour,
								 float _emitRate, float _startSize, float _endSize)
{
	Initialize(_particles, _position, _startColour, _endColour, _emitRate, _startSize, _endSize);
}

void ParticleEmitter::Create()
{
	m_shader->LoadProgram(nullptr, "./shader/particleMain.vs", "./shader/particleMain.fs");

	unsigned int* indexData = new unsigned int[m_maxParticles * 6];

	for (unsigned int i = 0; i < m_maxParticles; ++i)
	{
		indexData[i * 6 + 0] = i * 4 + 0;
		indexData[i * 6 + 1] = i * 4 + 1;
		indexData[i * 6 + 2] = i * 4 + 2;

		indexData[i * 6 + 3] = i * 4 + 0;
		indexData[i * 6 + 4] = i * 4 + 2;
		indexData[i * 6 + 5] = i * 4 + 3;
	}

	glGenVertexArrays(1, &(m_shaderData->m_VAO));
	glBindVertexArray(m_shaderData->m_VAO);

	glGenBuffers(1, &(m_shaderData->m_VBO));
	glGenBuffers(1, &(m_shaderData->m_IBO));

	glBindBuffer(GL_ARRAY_BUFFER, m_shaderData->m_VBO);
	glBufferData(GL_ARRAY_BUFFER, m_maxParticles * 4 * sizeof(ParticleVertex), m_vertexData, GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_shaderData->m_IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_maxParticles * 6 * sizeof(unsigned int), indexData, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0); //Position
	glEnableVertexAttribArray(1); //Colour

	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(ParticleVertex), 0);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(ParticleVertex), ((char*)0) + 16);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	
	delete[] indexData;
}

void ParticleEmitter::Initialize(Particle* _particles, vec3 _position,
							vec4 _startColour, vec4 _endColour,
							float _emitRate, float _startSize, float _endSize)
{
	m_position = _position;

	m_startColour = _startColour;
	m_endColour = _endColour;

	m_emitRate = 1.f / _emitRate;
	m_emitTimer = 0;

	m_firstDead = 0;

	m_lifeSpanMin = 0;
	m_lifeSpanMax = 5;

	m_startSize = _startSize;
	m_endSize = _endSize;

	m_velocityMin = 15.f;
	m_velocityMax = 20.f;

	m_maxParticles = 1000;

	m_shader = new Shader();
	m_shaderData = new ShaderData();

	m_particles =  new Particle[m_maxParticles];
	m_vertexData = new ParticleVertex[m_maxParticles * 4];

	Create();
}

void ParticleEmitter::Emit()
{
	if (m_firstDead >= m_maxParticles)
	{
		return;
	}

	Particle& particle = m_particles[m_firstDead++];

	particle.position = m_position;

	particle.lifetime = 0;
	particle.lifespan = (rand() / (float)RAND_MAX) * (m_lifeSpanMax - m_lifeSpanMin) + m_lifeSpanMin;

	particle.colour = m_startColour;
	particle.size = m_startSize;

	float velocity = (rand() / (float)RAND_MAX) * (m_velocityMax - m_velocityMin) + m_velocityMin;

	particle.velocity.x = (rand() / (float)RAND_MAX) * 2 - 1;
	particle.velocity.y = (rand() / (float)RAND_MAX) * 2 - 1;
	particle.velocity.z = (rand() / (float)RAND_MAX) * 2 - 1;

	particle.velocity = glm::normalize(particle.velocity) * velocity;
}

void ParticleEmitter::Update(float _deltaTime, FreeCamera* _camera)
{
	m_emitTimer += _deltaTime;

	while (m_emitTimer > m_emitRate)
	{
		Emit();
		m_emitTimer -= m_emitRate;
	}

	unsigned int quad = 0;

	for (unsigned int i = 0; i < m_firstDead; i++)
	{
		Particle* particle = &m_particles[i];

		particle->lifetime += _deltaTime;

		if (particle->lifetime >= particle->lifespan)
		{
			*particle = m_particles[m_firstDead - 1];
			m_firstDead--;
		}
		else
		{
			particle->position += particle->velocity * _deltaTime;

			particle->size = mix(m_startSize, m_endSize, particle->lifetime / particle->lifespan);
			particle->colour = mix(m_startColour, m_endColour, particle->lifetime / particle->lifespan);

			float halfSize = particle->size * 0.5f;

			m_vertexData[quad * 4 + 0].position = vec4(halfSize, halfSize, 0, 1);
			m_vertexData[quad * 4 + 0].colour = particle->colour;

			m_vertexData[quad * 4 + 1].position = vec4(-halfSize, halfSize, 0, 1);
			m_vertexData[quad * 4 + 1].colour = particle->colour;

			m_vertexData[quad * 4 + 2].position = vec4(-halfSize, -halfSize, 0, 1);
			m_vertexData[quad * 4 + 2].colour = particle->colour;

			m_vertexData[quad * 4 + 3].position = vec4(halfSize, -halfSize, 0, 1);
			m_vertexData[quad * 4 + 3].colour = particle->colour;

			vec3 zAxis = normalize(vec3(_camera->GetTransform()[3]) - particle->position);
			vec3 xAxis = cross(vec3(_camera->GetTransform()[1]), zAxis);
			vec3 yAxis = cross(zAxis, xAxis);
			mat4 billboard(	vec4(xAxis, 0),
							vec4(yAxis, 0),
							vec4(zAxis, 0),
							vec4(0, 0, 0, 1));

			m_vertexData[quad * 4 + 0].position = billboard * m_vertexData[quad * 4 + 0].position + vec4(particle->position, 0);
			m_vertexData[quad * 4 + 1].position = billboard * m_vertexData[quad * 4 + 1].position + vec4(particle->position, 0);
			m_vertexData[quad * 4 + 2].position = billboard * m_vertexData[quad * 4 + 2].position + vec4(particle->position, 0);
			m_vertexData[quad * 4 + 3].position = billboard * m_vertexData[quad * 4 + 3].position + vec4(particle->position, 0);

			++quad;

		}
	}
}
void ParticleEmitter::Draw(FreeCamera* _camera)
{
	glUseProgram(m_shader->m_programID);

	unsigned int view_proj_uniform = glGetUniformLocation(m_shader->m_programID, "ProjectionView");
	glUniformMatrix4fv(view_proj_uniform, 1, GL_FALSE, (float*)&_camera->GetProjectionView()[0][0]);

	glBindBuffer(GL_ARRAY_BUFFER, m_shaderData->m_VBO);
	glBufferSubData(GL_ARRAY_BUFFER, 0, m_firstDead * 4 * sizeof(ParticleVertex), m_vertexData);

	//Draw the particles! :D
	glBindVertexArray(m_shaderData->m_VAO);
	glDrawElements(GL_TRIANGLES, m_firstDead * 6, GL_UNSIGNED_INT, 0);
}