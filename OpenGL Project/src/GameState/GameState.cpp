#include "GameState.h"
#include "GameStateManager.h"

GameState::GameState(GLFWwindow* _window, GameStateManager* _manager, unsigned int _id)
{
	m_window = _window;
	m_manager = _manager;
	m_id = _id;
	m_manager->AddState(this);
}

void GameState::Update(float _deltaTime)
{

}

void GameState::Draw()
{

}

void GameState::InitializeTweak(char* _barName)
{
	m_tweakBarName = _barName;

	m_tweakBar = TwNewBar(m_tweakBarName);

	char buffer[256];

	strncpy(buffer, m_tweakBarName, sizeof(buffer));
	strncat(buffer, " visible=false", sizeof(buffer));

	TwDefine(buffer);
	TwAddButton(m_tweakBar, "Next State", GameStateManager::Static_NextState, (void*)m_manager, "group='State Manager'");
	TwAddButton(m_tweakBar, "Previous State", GameStateManager::Static_PreviousState, (void*)m_manager, "group='State Manager'");
}

void GameState::SetTweakActive()
{
	char buffer[256];

	strncpy(buffer, m_tweakBarName, sizeof(buffer));
	strncat(buffer, " visible=true", sizeof(buffer));

	TwDefine(buffer);
}

void GameState::SetTweakInactive()
{
	char buffer[256];

	strncpy(buffer, m_tweakBarName, sizeof(buffer));
	strncat(buffer, " visible=false", sizeof(buffer));

	TwDefine(buffer);
}