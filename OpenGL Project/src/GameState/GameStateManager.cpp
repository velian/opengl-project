#include "GameStateManager.h"
#include "GameState.h"
#include <iostream>
#include <gl_core_4_4.h>
#include <GLFW/glfw3.h>

void GameStateManager::SetContext(GLFWwindow* _window)
{
	m_window = _window;

	m_currentState = 1;
	m_previousState = 1;
	m_stateList = std::vector<GameState*>();

	//Add your states here
	m_splashState = new SplashState(m_window, this, 1);
	m_titleState = new TitleState(m_window, this, 2);

	AddState(m_splashState);
	AddState(m_titleState);
}

void GameStateManager::AddState(GameState* _state)
{
	m_stateList.push_back(_state);
}

unsigned int GameStateManager::GetState()
{
	return m_currentState;
}

void GameStateManager::SetState(unsigned int _state)
{
	m_previousState = m_currentState;
	m_currentState = _state;
}

void GameStateManager::NextState()
{
	if (m_currentState + 1 < m_stateList.size() - 1)
	{
		m_stateList[m_currentState - 1]->SetTweakInactive();
		m_stateList[m_currentState]->SetTweakActive();
		m_previousState = m_currentState;
		m_currentState = GetState() + 1;
	}	
}

void GameStateManager::PreviousState()
{
	if (m_currentState - 1 > 0)
	{
		m_stateList[m_currentState - 1]->SetTweakInactive();
		m_stateList[m_currentState]->SetTweakActive();
		m_previousState = m_currentState;
		m_currentState = GetState() - 1;
	}
}

void GameStateManager::Static_NextState(void* pData)
{
	GameStateManager* pThis = (GameStateManager*)pData;
	pThis->NextState();
}

void GameStateManager::Static_PreviousState(void* _data)
{
	GameStateManager* pThis = (GameStateManager*)_data;
	pThis->PreviousState();
}

unsigned int GameStateManager::GetStateSize()
{
	return m_stateList.size();
}

void GameStateManager::Update(float _deltaTime)
{
	for (unsigned int i = 0; i < m_stateList.size(); i++)
	{
		if (m_stateList[i]->m_id != m_currentState)
		{
			continue;
		}
		else
		{
			m_stateList[i]->Update(_deltaTime);
			return;
		}
	}
}

void GameStateManager::Draw()
{
	for (unsigned int i = 0; i < m_stateList.size(); i++)
	{
		if (m_stateList[i]->m_id != m_currentState)
		{
			continue;
		}
		else
		{			
			m_stateList[i]->Draw();
			return;
		}
	}
}