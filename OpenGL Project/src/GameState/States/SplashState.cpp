#include <iostream>
#include <gl_core_4_4.h>
#include <GLFW/glfw3.h>
#include "SplashState.h"
#include "GameStateManager.h"

//Temporary//
#include "Object.h"
#include "Shader.h"
#include "Camera.h"
#include "Texture.h"
#include "FBXObject.h"
#include "ParticleEmitter.h"
#include "GPUParticleEmitter.h"
#include "RenderTarget.h"
#include "Icon.h"
#include "SoundManager.h"
#include "GUIObject.h"
#include "GUIModShader.h"
//----------//

SplashState::SplashState(GLFWwindow* _window, GameStateManager* _manager, unsigned int _id) : GameState(_window, _manager, _id)
{
	InitializeTweak("SplashState");

	m_camera = new FreeCamera(10,0.5);
	m_camera->SetInputWindow(_window);
	m_camera->SetupPerspective(3.14159f * 0.25f, 4.0f / 3.0f);
	m_camera->SetPosition(glm::vec3(15, 1, 15));
	m_camera->LookAt(glm::vec3(100), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));

	m_texture = new Texture();
	m_texture->Initialize("./images/icons/heart.png", DRAW_TYPE::NEAREST);

	m_guiObject = new GUIObject("./images/icons/layout_delete.png", _window);
	m_guiObject->SetStartPoint(TOP_LEFT + glm::vec2(-200, 0), true);
	m_guiObject->SetEndPoint(TOP_LEFT);
	m_guiObject->SetDimensions(glm::vec2(75, 75));

	m_shaderMods = new GUIModShader("./images/icons/page.png", _window);
	m_shaderMods->SetStartPoint(TOP_RIGHT + glm::vec2(200, 0), true);
	m_shaderMods->SetEndPoint(TOP_RIGHT);
	m_shaderMods->SetDimensions(glm::vec2(75, 75));	

	guiList = new GUIObject[10]();

	for (int i = 1; i < 11; i++)
	{
		guiList[i - 1] = GUIObject("./images/icons/brick.png", _window);
		guiList[i - 1].SetStartPoint(TOP_LEFT + glm::vec2(-200, 0) + glm::vec2(0, -50 + (i * -75)));
		guiList[i - 1].SetEndPoint(TOP_LEFT + glm::vec2(-25, -50 + (i * -75)));
		guiList[i - 1].SetDimensions(glm::vec2(25, 25));
		guiList[i - 1].AttatchTo(m_guiObject);
	}

	m_gpuEmitter = new GPUParticleEmitter(100000, glm::vec3(0, 0, 0), m_texture);

	m_renderTarget = new RenderTarget(POST_TYPE::NONE, glm::vec3(0, 0, 0), glm::vec3(1, 1, 0));

	//m_renderTargetBoxBlur = new RenderTarget(POST_TYPE::BOX_BLUR, glm::vec3(0,0,0), glm::vec3(1, 1, 0));
	//m_renderTargetSharpen = new RenderTarget(POST_TYPE::PATCH, glm::vec3(0,0,0), glm::vec3(1, 1, 0));
	

	m_icon = new Icon("./images/pug.png", DRAW_TYPE::NEAREST);

	//m_shaderMods->AttatchShader(m_gpuEmitter->GetShader());

	//m_soundManager = new SoundManager();
	//m_soundManager->AddSound3D("./sound/music.ogg", "Music");
	//m_soundManager->PlaySound3D("Music");

	//m_object = new Object(this,	"./objects/commando1/DRBC.obj",
	//								"./objects/commando1/DRBC.png",
	//								"./objects/commando1/DRBC_GLOW.png");

	//m_object->Create();
}

void SplashState::Update(float _deltaTime)
{
	if (glfwGetKey(m_window, GLFW_KEY_F1) == GLFW_PRESS)
	{
		SetTweakActive();
	}
	
	m_camera->Update(_deltaTime);

	if (m_guiObject->OnMousePress() && m_guiObject->GetTexture()->GetCurrentPath() == "./images/icons/layout_delete.png")
	{
		m_guiObject->GetTexture()->Initialize("./images/icons/layout_add.png", DRAW_TYPE::NEAREST);
		m_guiObject->DisableChildren();
	}
	else if (m_guiObject->OnMousePress() && m_guiObject->GetTexture()->GetCurrentPath() == "./images/icons/layout_add.png")
	{
		m_guiObject->GetTexture()->Initialize("./images/icons/layout_delete.png", DRAW_TYPE::NEAREST);
		m_guiObject->EnableChildren();
	}
	else if (guiList[0].OnMousePress())
	{
		guiList[0].Disable();
	}
	else if (guiList[1].OnMousePress())
	{
		m_renderTarget->GetShader()->OpenDialogue(&m_renderTarget->GetShader()->m_programID, SHADER_TYPE::VERTEX, SHADER_TYPE::FRAGMENT);
	}	
	else if (guiList[2].OnMousePress())
	{
		m_icon->GetTexture()->OpenDialogue(DRAW_TYPE::NEAREST);
	}

	for (int i = 0; i < 10; i++)
	{
		guiList[i].Update(_deltaTime);
	}
	

	//	m_shaderMods->Update(_deltaTime);

	m_gpuEmitter->Update(_deltaTime);
	m_icon->Update(m_camera);

	m_guiObject->Update(_deltaTime);
	m_renderTarget->Update(m_camera);
	//m_renderTargetBoxBlur->Update(m_camera);
	//m_renderTargetSharpen->Update(m_camera);

	//m_fbx->Update(_deltaTime);
}

void SplashState::Draw()
{	
	m_renderTarget->SetAsActiveFrameBuffer();
	glClearColor(0, 0, 0, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_guiObject->Draw(m_camera);

	for (int i = 0; i < 10; i++)
	{
		guiList[i].Draw(m_camera);
	}

	//m_shaderMods->Draw(m_camera);

	m_gpuEmitter->Draw(m_camera);
	m_icon->Draw(m_camera);

	m_renderTarget->ClearAsActiveFrameBuffer();

	m_renderTarget->Draw(m_camera);

	//
	//m_renderTargetSharpen->SetAsActiveFrameBuffer();
	//
	//m_renderTargetBoxBlur->Draw(m_camera);
	//
	//m_renderTargetSharpen->ClearAsActiveFrameBuffer();
	//
	//m_renderTargetSharpen->Draw(m_camera);
}