#include "TitleState.h"
#include "GameStateManager.h"
#include <iostream>
#include <gl_core_4_4.h>
#include <GLFW/glfw3.h>
#include "glm/glm.hpp"

TitleState::TitleState(GLFWwindow* _window, GameStateManager* _manager, unsigned int _id) : GameState(_window, _manager, _id)
{
	InitializeTweak("TitleState");
}

void TitleState::Initialize(GLFWwindow* _window, GameStateManager* _manager, unsigned int _id)
{
	m_id = _id;
}

void TitleState::Update(float _deltaTime)
{

}

void TitleState::Draw()
{
	
}