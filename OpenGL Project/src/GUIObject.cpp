#include <gl_core_4_4.h>
#include <GLFW/glfw3.h>

#include <glm\ext.hpp>

#include "GUIObject.h"
#include "Texture.h"
#include "Camera.h"

GUIObject::GUIObject()
{
	
}

GUIObject::GUIObject(char* _filePath, GLFWwindow* _window)
{
	Initialize(_filePath, _window);
}

void GUIObject::Initialize(char* _filePath, GLFWwindow* _window)
{
	m_window = _window;

	m_enabled = true;
	m_justEnabled = true;

	m_lerpTimer = 0.f;

	m_shader = new Shader();
	m_shaderData = new ShaderData();
	m_texture = new Texture();	

	m_parent = nullptr;
	m_children = std::vector<GUIObject*>();

	m_guiData = new GUIData[4];

	SetDimensions(glm::vec2(50, 50));
	SetPosition(glm::vec2(0, 0));

	SetStartPoint(m_position);
	SetEndPoint(TOP_LEFT);

	m_texture->Initialize(_filePath, DRAW_TYPE::NEAREST);
	
	Create();
}

void GUIObject::SetPosition(glm::vec2 _position, bool _convertToDecimal)
{
	int width, height;
	glfwGetWindowSize(m_window, &width, &height);

	m_position = glm::vec2(_position.x / width, _position.y / height);
}

glm::vec2 GUIObject::GetPosition()
{
	return m_position;
}

void GUIObject::SetDimensions(glm::vec2 _dimensions)
{
	int width, height;
	glfwGetWindowSize(m_window, &width, &height);

	m_dimensions = glm::vec2(_dimensions.x / width, _dimensions.y / height);
}

glm::vec2 GUIObject::GetDimensions()
{
	return m_dimensions;
}

bool GUIObject::OnMouseOver()
{
	double x, y;
	int width, height;
	glfwGetCursorPos(m_window, &x, &y);
	glfwGetWindowSize(m_window, &width, &height);

	x -= width / 2;
	y -= height / 2;

	int posX  = (int)( m_position.x	  * width / 2);
	int posY  = (int)(-m_position.y	  * height / 2);
	int sizeX = (int)( m_dimensions.x * width);
	int sizeY = (int)( m_dimensions.y * height);

	//printf("Mouse Position : %i - %i\n", (int)x, (int)y);

	if (x > posX - (sizeX / 2) && x < posX + (sizeX / 2) &&
		y > posY - (sizeY / 2) && y < posY + (sizeY / 2))
	{
		//printf("COLLISION!\n");
		return true;
	}
	return false;
}

bool GUIObject::OnMousePress()
{
	if (OnMouseOver() && glfwGetMouseButton(m_window, 0) && m_mouseJustPressed)
	{
		return true;
	}
	return false;
}

bool GUIObject::IsEnabled()
{
	return m_enabled;
}

void GUIObject::Enable()
{
	if (m_parent != nullptr)
	{
		if (m_parent->IsEnabled())
			m_enabled = true;
	}
}

void GUIObject::Disable()
{
	if (m_children.size() > 0)
	{
		for (unsigned int i = 0; i < m_children.size(); i++)
		{
			m_children[i]->Disable();
		}
	}
	m_enabled = false;
}

void GUIObject::EnableChildren()
{
	if (m_children.size() > 0)
	{
		for (unsigned int i = 0; i < m_children.size(); i++)
		{
			m_children[i]->Enable();
		}
	}
}

void GUIObject::DisableChildren()
{
	if (m_children.size() > 0)
	{
		for (unsigned int i = 0; i < m_children.size(); i++)
		{
			m_children[i]->Disable();
		}
	}
}

void GUIObject::SetStartPoint(glm::vec2 _position, bool _setPosition)
{
	int width, height;
	glfwGetWindowSize(m_window, &width, &height);

	m_startPoint = glm::vec2(_position.x / width, _position.y / height);

	if (_setPosition)
	{
		SetPosition(m_startPoint);
	}
}

void GUIObject::SetEndPoint(glm::vec2 _position)
{
	int width, height;
	glfwGetWindowSize(m_window, &width, &height);

	m_endPoint = glm::vec2(_position.x / width, _position.y / height);
}

glm::vec2 GUIObject::GetStartPoint()
{
	return m_startPoint;
}

glm::vec2 GUIObject::GetEndPoint()
{
	return m_endPoint;
}

//Attatch to object as child
void GUIObject::AttatchTo(GUIObject* _object)
{
	_object->Attatch(this);
}

void GUIObject::DetatchFrom()
{
	m_parent->Detatch(this);
}

//Attatch object as child
void GUIObject::Attatch(GUIObject* _object)
{
	_object->m_parent = this;
	m_children.push_back(_object);
}

void GUIObject::Detatch(GUIObject* _object)
{
	_object->m_parent = nullptr;
	m_children.erase(std::remove(m_children.begin(), m_children.end(), _object), m_children.end());
}

void GUIObject::Create()
{
	m_shader->LoadProgram(nullptr, "./shader/guiObject.vs", "./shader/guiObject.fs");
	
	m_guiData[0] = { glm::vec2(m_position.x + -m_dimensions.x, m_position.y + -m_dimensions.y),  glm::vec2(0, 1) };
	m_guiData[1] = { glm::vec2(m_position.x +  m_dimensions.x, m_position.y + -m_dimensions.y),  glm::vec2(1, 1) };
	m_guiData[2] = { glm::vec2(m_position.x +  m_dimensions.x, m_position.y +  m_dimensions.y),  glm::vec2(1, 0) };
	m_guiData[3] = { glm::vec2(m_position.x + -m_dimensions.x, m_position.y +  m_dimensions.y),  glm::vec2(0, 0) };

	unsigned int indexData[] = {
		0, 1, 2,
		0, 2, 3,
	};

	glGenVertexArrays(1, &(m_shaderData->m_VAO));
	glBindVertexArray(m_shaderData->m_VAO);

	glGenBuffers(1, &(m_shaderData->m_VBO));
	glGenBuffers(1, &(m_shaderData->m_IBO));

	glBindBuffer(GL_ARRAY_BUFFER, m_shaderData->m_VBO);
	glBufferData(GL_ARRAY_BUFFER, 4 * sizeof(GUIData), m_guiData, GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_shaderData->m_IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * sizeof(unsigned int), indexData, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0); //Position
	glEnableVertexAttribArray(1); //Texcoord

	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(GUIData), 0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(GUIData), ((char*)0) + 8);

	glBindVertexArray(0);
}

void GUIObject::HandleMousePress()
{
	if (glfwGetMouseButton(m_window, 0) && m_mouseJustPressed != true && m_mousePressed != true)
	{
		m_mouseJustPressed = true;
	}
	else if (glfwGetMouseButton(m_window, 0) && m_mouseJustPressed == true)
	{
		m_mouseJustPressed = false;
		m_mousePressed = true;
	}
	else if (!glfwGetMouseButton(m_window, 0) && m_mouseJustPressed != true && m_mousePressed == true)
	{
		m_mousePressed = false;
	}
}

void GUIObject::Update(float _deltaTime)
{
	EnableLerp(_deltaTime);

	m_guiData[0] = { glm::vec2(m_position.x + -m_dimensions.x, m_position.y + -m_dimensions.y),  glm::vec2(0, 1) };
	m_guiData[1] = { glm::vec2(m_position.x +  m_dimensions.x, m_position.y + -m_dimensions.y),  glm::vec2(1, 1) };
	m_guiData[2] = { glm::vec2(m_position.x +  m_dimensions.x, m_position.y +  m_dimensions.y),  glm::vec2(1, 0) };
	m_guiData[3] = { glm::vec2(m_position.x + -m_dimensions.x, m_position.y +  m_dimensions.y),  glm::vec2(0, 0) };

	HandleMousePress();
}

void GUIObject::Draw(FreeCamera* _camera)
{
	if (m_enabled)
	{
		glUseProgram(m_shader->m_programID);

		if (GetTexture() != nullptr)
		{
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, GetTexture()->GetID());

			int location = glGetUniformLocation(m_shader->m_programID, "diffuse");
			glUniform1i(location, 0);
		}

		glBindBuffer(GL_ARRAY_BUFFER, m_shaderData->m_VBO);
		glBufferSubData(GL_ARRAY_BUFFER, 0, 4 * sizeof(GUIData), m_guiData);

		//Draw the icon
		glBindVertexArray(m_shaderData->m_VAO);
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

		glBindVertexArray(0);
		glUseProgram(0);
	}
}

void GUIObject::EnableLerp(float _deltaTime)
{
	if (m_justEnabled == true && m_enabled == true)
	{
		if (m_position == m_endPoint || m_lerpTimer > 1)
		{
			m_lerpTimer = 0;
			m_justEnabled = false;
		}
		else
		{
			m_position = glm::lerp(m_startPoint, m_endPoint, m_lerpTimer);

			m_lerpTimer += _deltaTime;
		}
	}
}