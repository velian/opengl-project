#include <gl_core_4_4.h>
#include <GLFW/glfw3.h>
#include "Shader.h"
#include "Object.h"
#include "ObjectData.h"
#include "Texture.h"
#include "Camera.h"
#include "GameState.h"
#include "FBXObject.h"
#include "Light.h"

#include "glm\gtc\type_ptr.hpp"

Shader::Shader()
{
	Initialize();
}

Shader::Shader(GameState* _gameState)
{
	Initialize();
}

void Shader::Initialize()
{
	m_programID = 0;
}

void Shader::Draw(FreeCamera* _camera, Object* _object)
{
	glUseProgram(m_programID);

	int view_proj_uniform = glGetUniformLocation(m_programID, "ProjectionView");
	glUniformMatrix4fv(view_proj_uniform, 1, GL_FALSE, (float*)&_camera->GetProjectionView()[0][0]);

	unsigned int CameraPos = glGetUniformLocation(m_programID, "CameraPos");
	glUniform3f(CameraPos, _camera->GetPosition().x, _camera->GetPosition().y, _camera->GetPosition().z);

	if (_object->GetTexture() != nullptr)
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, _object->GetTexture()->GetID());

		view_proj_uniform = glGetUniformLocation(m_programID, "diffuse");
		glUniform1i(view_proj_uniform, 0);
	}

	if (_object->GetOverlay() != nullptr)
	{
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, _object->GetOverlay()->GetID());

		view_proj_uniform = glGetUniformLocation(m_programID, "overlay");
		glUniform1i(view_proj_uniform, 1);
	}

	if (_object->GetLight() != nullptr)
	{
		unsigned int specular = glGetUniformLocation(m_programID, "SpecPow");
		glUniform1f(specular, _object->GetLight()->m_specular / 100);

		unsigned int ambientLight = glGetUniformLocation(m_programID, "AmbientLight");
		glUniform1f(ambientLight, _object->GetLight()->m_ambientLight / 100);

		unsigned int LightColour = glGetUniformLocation(m_programID, "LightColour");
		glUniform3f(LightColour, _object->GetLight()->m_lightColour.x, _object->GetLight()->m_lightColour.y, _object->GetLight()->m_lightColour.z);

		unsigned int LightDir = glGetUniformLocation(m_programID, "LightDir");
		glUniform3f(LightDir, -_object->GetLight()->m_lightDirection.x, -_object->GetLight()->m_lightDirection.y, -_object->GetLight()->m_lightDirection.z);
	}

	for (unsigned int i = 0; i < _object->GetData()->m_objectShapeSize; ++i)
	{
		glBindVertexArray(_object->GetShaderData()[i].m_VAO);
		glDrawElements(GL_TRIANGLES, _object->GetData()->m_indexCount, GL_UNSIGNED_INT, 0);
	}
}

void Shader::Draw(FreeCamera* _camera, FBXObject* _object)
{
	glUseProgram(m_programID);

	unsigned int view_proj_uniform = glGetUniformLocation(m_programID, "ProjectionView");
	glUniformMatrix4fv(view_proj_uniform, 1, GL_FALSE, (float*)&_camera->GetProjectionView()[0][0]);

	unsigned int CameraPos = glGetUniformLocation(m_programID, "CameraPos");
	glUniform3f(CameraPos, _camera->GetPosition().x, _camera->GetPosition().y, _camera->GetPosition().z);	

	if (_object->GetFile()->getSkeletonCount() > 0)
	{
		FBXSkeleton* skeleton = _object->GetFile()->getSkeletonByIndex(0);
		skeleton->updateBones();

		for (unsigned int i = 0; i < skeleton->m_boneCount; i++)
		{
			std::string boneName = "Bones[" + std::to_string(i) + "]";
			unsigned int bones_location = glGetUniformLocation(m_programID, boneName.c_str());
			glUniformMatrix4fv(bones_location, 1, GL_FALSE, glm::value_ptr(skeleton->m_bones[i]));
		}
	}

	if (_object->GetLight() != nullptr)
	{
		unsigned int specular = glGetUniformLocation(m_programID, "SpecPow");
		glUniform1f(specular, _object->GetLight()->m_specular / 100);

		unsigned int ambientLight = glGetUniformLocation(m_programID, "AmbientLight");
		glUniform1f(ambientLight, _object->GetLight()->m_ambientLight / 100);

		unsigned int LightColour = glGetUniformLocation(m_programID, "LightColour");
		glUniform3f(LightColour, _object->GetLight()->m_lightColour.x, _object->GetLight()->m_lightColour.y, _object->GetLight()->m_lightColour.z);

		unsigned int LightDir = glGetUniformLocation(m_programID, "LightDir");
		glUniform3f(LightDir, -_object->GetLight()->m_lightDirection.x, -_object->GetLight()->m_lightDirection.y, -_object->GetLight()->m_lightDirection.z);
	}

	for (unsigned int i = 0; i < _object->GetFile()->getMeshCount(); i++)
	{
		FBXMeshNode* pMesh = _object->GetFile()->getMeshByIndex(i);
		ShaderData* pMeshData = (ShaderData*)pMesh->m_userData;

		unsigned int global_uniform = glGetUniformLocation(m_programID, "global");
		glUniformMatrix4fv(global_uniform, 1, GL_FALSE, (float*)&pMesh->m_globalTransform[0][0]);

		if (pMesh->m_material->textures[FBXMaterial::DiffuseTexture] != nullptr)
		{
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, pMesh->m_material->textures[FBXMaterial::DiffuseTexture]->handle);
		}
		
		if (pMesh->m_material->textures[FBXMaterial::NormalTexture] != nullptr)
		{
			glActiveTexture(GL_TEXTURE1);
			glBindTexture(GL_TEXTURE_2D, pMesh->m_material->textures[FBXMaterial::NormalTexture]->handle);
		}

		if (pMesh->m_material->textures[FBXMaterial::SpecularTexture] != nullptr)
		{
			glActiveTexture(GL_TEXTURE2);
			glBindTexture(GL_TEXTURE_2D, pMesh->m_material->textures[FBXMaterial::SpecularTexture]->handle);
		}

		if (pMesh->m_material->textures[FBXMaterial::GlowTexture] != nullptr)
		{
			glActiveTexture(GL_TEXTURE3);
			glBindTexture(GL_TEXTURE_2D, pMesh->m_material->textures[FBXMaterial::GlowTexture]->handle);
		}

		if (pMesh->m_material->textures[FBXMaterial::GlossTexture] != nullptr)
		{
			glActiveTexture(GL_TEXTURE4);
			glBindTexture(GL_TEXTURE_2D, pMesh->m_material->textures[FBXMaterial::GlossTexture]->handle);
		}

		glBindVertexArray(pMeshData->m_VAO);
		glDrawElements(GL_TRIANGLES, (unsigned int)pMesh->m_indices.size(), GL_UNSIGNED_INT, 0);
	}
}

void Shader::LoadProgram(char** _varyings, char* _vsPath, char* _fsPath, char* _gsPath)
{
	unsigned int vertexShaderId   = _vsPath ? LoadShader(_vsPath, GL_VERTEX_SHADER) : NULL;
	unsigned int fragmentShaderId = _fsPath ? LoadShader(_fsPath, GL_FRAGMENT_SHADER) : NULL;
	unsigned int geometryShaderId = _gsPath ? LoadShader(_gsPath, GL_GEOMETRY_SHADER) : NULL;

	unsigned int programID = CreateProgram(_varyings, vertexShaderId, fragmentShaderId, geometryShaderId);

	glDeleteShader(vertexShaderId);
	glDeleteShader(fragmentShaderId);
	glDeleteShader(geometryShaderId);

	m_programID = programID;
}

unsigned int Shader::LoadShader(char* _filePath, unsigned int _type)
{
	int success = GL_FALSE;

	unsigned int handle = glCreateShader(_type);
	unsigned char* source = FileToBuffer(_filePath);

	glShaderSource(handle, 1, (const char**)&source, 0);
	glCompileShader(handle);

	glGetShaderiv(handle, GL_COMPILE_STATUS, &success);

	if (success == GL_FALSE)
	{
		int infoLength = 0;
		glGetShaderiv(handle, GL_INFO_LOG_LENGTH, &infoLength);
		char* infoLog = new char[infoLength];

		glGetShaderInfoLog(handle, infoLength, 0, infoLog);
		printf("Faliled to compile shader program : %s", infoLog);
		delete[] infoLog;

		return 0;
	}

	delete[] source;

	return handle;
}

unsigned char* Shader::FileToBuffer(char* _filePath)
{
	FILE* file = fopen(_filePath, "rb");

	if (file == nullptr)
	{
		printf("Couldn't open file : %s", _filePath);
		return nullptr;
	}

	// get number of bytes in file
	fseek(file, 0, SEEK_END);
	unsigned int uiLength = (unsigned int)ftell(file);
	fseek(file, 0, SEEK_SET);

	// allocate buffer and read file contents
	unsigned char* acBuffer = new unsigned char[uiLength + 1];
	memset(acBuffer, 0, uiLength + 1);
	fread(acBuffer, sizeof(unsigned char), uiLength, file);

	fclose(file);
	return acBuffer;
}

unsigned int Shader::CreateProgram(char** _varyings, GLuint _vs, GLuint _fs, GLuint _gs)
{
	int success = GL_FALSE;

	unsigned int handle = glCreateProgram();
	if (_vs != NULL) glAttachShader(handle, _vs);
	if (_fs != NULL) glAttachShader(handle, _fs);
	if (_gs != NULL) glAttachShader(handle, _gs);

	if (_varyings != nullptr)
	{
		glTransformFeedbackVaryings(handle, 4, _varyings, GL_INTERLEAVED_ATTRIBS);
	}

	glLinkProgram(handle);
	glGetProgramiv(handle, GL_LINK_STATUS, &success);

	if (success == GL_FALSE)
	{
		int infoLength = 0;
		glGetProgramiv(handle, GL_INFO_LOG_LENGTH, &infoLength);
		char* infoLog = new char[infoLength];

		glGetProgramInfoLog(handle, infoLength, 0, infoLog);
		printf("Faliled to link shader program : %s", infoLog);
		delete[] infoLog;

		return 0;
	}

	return handle;
}

void Shader::OpenDialogue(unsigned int* _shaderID, SHADER_TYPE _shaderType, SHADER_TYPE _secondaryShader)
{
	OPENFILENAME firstShader;
	OPENFILENAME secondShader;
	char szFileName[MAX_PATH] = "";

	ZeroMemory(&firstShader, sizeof(firstShader));
	ZeroMemory(&secondShader, sizeof(secondShader));

	firstShader.lStructSize = sizeof(firstShader);
	secondShader.lStructSize = sizeof(secondShader);

	switch (_shaderType)
	{
		case (SHADER_TYPE::VERTEX) :
		{
			firstShader.lpstrFilter = L"Vertex Shader (*.vs)\0*.vs";
			break;
		}
		case (SHADER_TYPE::FRAGMENT) :
		{
			firstShader.lpstrFilter = L"Fragment Shader (*.fs)\0*.fs";
			break;
		}
	}

	switch (_secondaryShader)
	{
		case (SHADER_TYPE::VERTEX) :
		{
			secondShader.lpstrFilter = L"Vertex Shader (*.vs)\0*.vs";
			break;
		}
		case (SHADER_TYPE::FRAGMENT) :
		{
			secondShader.lpstrFilter = L"Fragment Shader (*.fs)\0*.fs";
			break;
		}
	}

	firstShader.lpstrFile = (LPWSTR)szFileName;
	firstShader.nMaxFile = MAX_PATH;
	firstShader.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;

	secondShader.lpstrFile = (LPWSTR)szFileName;
	secondShader.nMaxFile = MAX_PATH;
	secondShader.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;
	//ofn.lpstrDefExt = L"txt";

	std::string vertexPath;
	std::string fragmentPath;

	if (GetOpenFileName(&firstShader))
	{
		vertexPath = CW2A(firstShader.lpstrFile);

		printf("Loaded Shader From File: %s", vertexPath.c_str());
	}

	if (GetOpenFileName(&secondShader))
	{
		fragmentPath = CW2A(secondShader.lpstrFile);

		printf("Loaded Shader From File: %s", fragmentPath.c_str());
	}
	else
		return;

	LoadProgram(nullptr, (char*)vertexPath.c_str(), (char*)fragmentPath.c_str());
}