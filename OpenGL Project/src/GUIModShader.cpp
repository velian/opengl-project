#include "GUIModShader.h"
#include "Texture.h"

GUIModShader::GUIModShader() : GUIObject()
{

}

GUIModShader::GUIModShader(char* _filePath, GLFWwindow* _window) : GUIObject(_filePath, _window)
{
	m_texture->Initialize(_filePath);
}

void GUIModShader::Update(float _deltaTime)
{
	if(OnMousePress())
	{
		m_shaderReference->OpenDialogue(&m_shaderReference->m_programID);
	}
}

void GUIModShader::AttatchShader(Shader* _shaderReference)
{
	m_shaderReference = _shaderReference;
}

void GUIModShader::DetatchShader()
{
	m_shaderReference = nullptr;
}