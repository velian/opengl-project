#ifndef GAME_STATE_H
#define GAME_STATE_H

#include "AntTweakBar.h"

class GameStateManager;
struct GLFWwindow;

class GameState
{
public:

	GameState(GLFWwindow* _window, GameStateManager* _manager, unsigned int _id);

	virtual void Update(float _deltaTime);
	virtual void Draw();	

	void SetTweakActive();
	void SetTweakInactive();

	TwBar* GetTweakBar(){ return m_tweakBar; }

	unsigned int m_id;

protected:

	void InitializeTweak(char* _barName);

	char* m_tweakBarName;

	GameStateManager* m_manager;
	GLFWwindow* m_window;
	TwBar* m_tweakBar;
};

#endif