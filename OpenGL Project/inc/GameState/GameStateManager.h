#ifndef GAME_STATE_MANAGER_H
#define GAME_STATE_MANAGER_H

#include <vector>
#include "SplashState.h"
#include "TitleState.h"

class GameState;
struct GLFWwindow;

class GameStateManager
{
public:

	GameStateManager(){}

	void Update(float _deltaTime);
	void Draw();

	void SetContext(GLFWwindow* _window);

	void AddState(GameState* _state);
	void SetState(unsigned int _state);

	void TW_CALL NextState();
	void TW_CALL PreviousState();

	static void TW_CALL Static_NextState(void* _data);
	static void TW_CALL Static_PreviousState(void* _data);

	unsigned int GetState();

	unsigned int GetStateSize();

protected:

	std::vector<GameState*> m_stateList;

	unsigned int m_currentState;
	unsigned int m_previousState;

	GLFWwindow* m_window;

	//List of states
	SplashState* m_splashState;
	TitleState* m_titleState;

};

#endif