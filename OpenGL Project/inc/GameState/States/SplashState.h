#ifndef SPLASH_STATE_H
#define SPLASH_STATE_H

#include "GameState.h"

struct GLFWwindow;
class Object;
class FreeCamera;
class Texture;
class FBXObject;
class Grid;
class ParticleEmitter;
class GPUParticleEmitter;
class Icon;
class SoundManager;
class RenderTarget;
class GUIObject;
class GUIModShader;

class SplashState : public GameState
{
public:

	SplashState(GLFWwindow* _window, GameStateManager* _manager, unsigned int _id);

	virtual void Update(float _deltaTime);
	virtual void Draw();

private:

	bool pressingMouse;

	FreeCamera* m_camera;
	Object* m_object;	
	Texture* m_texture;
	FBXObject* m_fbx;
	Grid* m_gridShape;
	ParticleEmitter* m_emitter;
	GPUParticleEmitter* m_gpuEmitter;
	Icon* m_icon;
	SoundManager* m_soundManager;
	RenderTarget* m_renderTarget;
	RenderTarget* m_renderTargetBoxBlur;
	RenderTarget* m_renderTargetSharpen;
	GUIObject* m_guiObject;
	GUIObject* guiList;
	GUIModShader* m_shaderMods;
};

#endif