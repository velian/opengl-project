#ifndef TEXTURE_H
#define TEXTURE_H

#include <gl_core_4_4.h>
#include <GLFW/glfw3.h>
#include "glm\glm.hpp"
#include "Shader.h"

class Camera;

enum DRAW_TYPE
{
	NEAREST = 0,
	LINEAR = 1
};

enum TEX_TYPE
{
	PNG = 0,
	JPG = 1,
	TGA = 2
};

class Texture
{
public:

	Texture();
	Texture(Shader* _shader){ m_shader = _shader; }

	void Initialize(std::string _filePath, DRAW_TYPE _drawType = LINEAR);

	virtual void Update(float _deltaTime);
	virtual void Draw(FreeCamera* _camera);

	void OpenDialogue(DRAW_TYPE _drawType = LINEAR);

	unsigned int GetID(){ return m_id; }
	std::string GetCurrentPath() { return m_currentPath; }

	unsigned int m_id;

private:

	Shader* m_shader;

	std::string m_currentPath;

	int m_imageWidth;
	int m_imageHeight;
	int m_imageFormat;

	unsigned char* m_data;
	
};

#endif