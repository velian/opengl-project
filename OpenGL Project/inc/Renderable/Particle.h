#ifndef PARTICLE_H
#define PARTICLE_H

#include "glm\glm.hpp"

struct ParticleVertex
{
	glm::vec4 position;
	glm::vec4 colour;
};

struct Particle
{
	glm::vec3 position;
	glm::vec3 velocity;
	glm::vec4 colour;
	float size;
	float lifetime;
	float lifespan;
};

#endif