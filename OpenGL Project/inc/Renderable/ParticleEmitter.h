#ifndef PARTICLE_EMITTER_H
#define PARTICLE_EMITTER_H

struct Particle;
struct ParticleVertex;
class Shader;
struct ShaderData;
class FreeCamera;

#include "glm\glm.hpp"

using namespace glm;
class ParticleEmitter
{
public:

	ParticleEmitter(Particle* _particles = nullptr, vec3 _position = vec3(0,0,0),
					vec4 _startColour = vec4(1,0,1,1), vec4 _endColour = vec4(0,1,0,1),
					float _emitRate = 25.f, float _startSize = 20.f, float _endSize = 10.f);

	void Create();
	void Emit();

	virtual void Update(float _deltaTime, FreeCamera* _camera);
	virtual void Draw(FreeCamera* _camera);

protected:

	void Initialize(Particle* _particles, vec3 _position,
					vec4 _startColour, vec4 _endColour,
					float _emitRate, float _startSize, float _endSize);

	//Variables//
	Particle* m_particles;
	ParticleVertex* m_vertexData;

	Shader* m_shader;
	ShaderData* m_shaderData;

	vec3 m_position;

	vec4 m_startColour;
	vec4 m_endColour;

	float m_emitTimer;
	float m_emitRate;

	float m_lifeSpanMin;
	float m_lifeSpanMax;

	float m_velocityMin;
	float m_velocityMax;

	float m_startSize;
	float m_endSize;

	unsigned int m_firstDead;
	unsigned int m_maxParticles;
};

#endif