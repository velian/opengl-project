#ifndef ICON_H
#define ICON_H

#include "glm\glm.hpp"

class Shader;
struct ShaderData;
class Texture;
class FreeCamera;
enum DRAW_TYPE;

struct IconData
{
	glm::vec4 position;
	glm::vec2 texcoord;
};

class Icon
{
public:

	Icon(char* _filePath, DRAW_TYPE _texType);
	
	void Update(FreeCamera* _camera);
	void Draw(FreeCamera* _camera);

	Shader* GetShader() { return m_shader; }
	Texture* GetTexture() { return m_texture; }

	void SetTexture(Texture* _texture){ m_texture = _texture; }

protected:

	void Initialize(char* _filePath, DRAW_TYPE _texType);

	void Create();

	Shader* m_shader;
	IconData* m_iconData;
	ShaderData* m_shaderData;
	Texture* m_texture;

	glm::vec3 m_position;
};

#endif