#ifndef GUIMODSHADER_H
#define GUIMODSHADER_H

#include "GUIObject.h"
#include "Shader.h"

class GUIModShader : public GUIObject
{
public:

	GUIModShader();
	GUIModShader(char* _filePath, GLFWwindow* _window);

	virtual void Update(float _deltaTime);

	void AttatchShader(Shader* _shaderReference);
	void DetatchShader();

protected:

	Shader* m_shaderReference;
};

#endif