#ifndef GUIOBJECT_H
#define GUIOBJECT_H

#include <glm\glm.hpp>
#include <vector>

class Shader;
struct ShaderData;
class Texture;
class FreeCamera;
struct GLFWwindow;
enum TEX_TYPE;

#define TOP_LEFT glm::vec2(-1210, 650)
#define BOTTOM_LEFT glm::vec2(-1210, -650)
#define TOP_RIGHT glm::vec2(1210, 650)
#define BOTTOM_RIGHT glm::vec2(1210, -650)

struct GUIData
{
	glm::vec2 position;
	glm::vec2 texcoord;
};

class GUIObject
{
public:

	GUIObject();
	GUIObject(char* _filePath, GLFWwindow* _window);

	virtual void Update(float _deltaTime);
	void Draw(FreeCamera* _camera);

	//GUI Stuff
	bool OnMouseOver();
	bool OnMousePress();

	bool IsEnabled();

	//Enable / Disable this object
	//Note - Disable calls disable on children also.
	void Enable();
	void Disable();

	//Enable / Disable children
	//Note - Enable only works if the parent is enabled
	void EnableChildren();
	void DisableChildren();

	//Attatch to object as child
	void AttatchTo(GUIObject* _object);
	void DetatchFrom();

	//Set start and end points : used during enable lerp
	void SetStartPoint(glm::vec2 _position, bool _setPosition = false);
	void SetEndPoint(glm::vec2 _position);

	glm::vec2 GetStartPoint();
	glm::vec2 GetEndPoint();

	//Attatch object as child
	void Attatch(GUIObject* _object);
	void Detatch(GUIObject* _object);	

	void SetPosition(glm::vec2 _position, bool _convertToDecimal = true);
	glm::vec2 GetPosition();

	void SetDimensions(glm::vec2 _dimensions);
	glm::vec2 GetDimensions();

	Shader* GetShader() { return m_shader; }
	Texture* GetTexture() { return m_texture; }

	void SetTexture(Texture* _texture){ m_texture = _texture; }

protected:

	void Initialize(char* _filePath, GLFWwindow* _window);

	void Create();
	void HandleMousePress();

	void EnableLerp(float _deltaTime);

	bool m_enabled;
	bool m_justEnabled;
	bool m_mousePressed;
	bool m_mouseJustPressed;

	float m_lerpTimer;

	glm::vec2 m_startPoint;
	glm::vec2 m_endPoint;

	Shader* m_shader;
	ShaderData* m_shaderData;
	Texture* m_texture;
	GUIData* m_guiData;

	GLFWwindow* m_window;

	GUIObject* m_parent;
	std::vector<GUIObject*> m_children;

	glm::vec2 m_position;
	glm::vec2 m_dimensions;
};

#endif