#ifndef TWEAK_FUNCTIONS_H
#define TWEAK_FUNCTIONS_H

#include "glm/glm.hpp"
#include <gl_core_4_4.h>
#include <GLFW/glfw3.h>

class TweakBarFunctions
{
public:
	static void OnMouseButton(GLFWwindow*, int b, int a, int m) {
		TwEventMouseButtonGLFW(b, a);
	}
	static void OnMousePosition(GLFWwindow*, double x, double y) {
		TwEventMousePosGLFW((int)x, (int)y);
	}
	static void OnMouseScroll(GLFWwindow*, double x, double y) {
		TwEventMouseWheelGLFW((int)y);
	}
	static void OnKey(GLFWwindow*, int k, int s, int a, int m) {
		TwEventKeyGLFW(k, a);
	}
	static void OnChar(GLFWwindow*, unsigned int c) {
		TwEventCharGLFW(c, GLFW_PRESS);
	}
	static void OnWindowResize(GLFWwindow*, int w, int h) {
		TwWindowSize(w, h);
		glViewport(0, 0, w, h);
	}
};
#endif