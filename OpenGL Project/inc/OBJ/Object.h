#ifndef OBJECT_H
#define OBJECT_H

#include "Shader.h"

struct ObjectData;
class FreeCamera;
class Texture;
class GameState;
class Light;

class Object
{
public:

	Object();
	Object(GameState* _gameState, char* _objPath);
	Object(GameState* _gameState, char* _objPath, char* _texPath);
	Object(GameState* _gameState, char* _objPath, char* _texPath, char* _glowPath);

	void Initialize(GameState* _gameState, char* _filePath);
	void Initialize(GameState* _gameState, char* _objPath, char* _texPath);
	void Initialize(GameState* _gameState, char* _objPath, char* _texPath, char* _normalPath);

	void Create();

	virtual void Update();
	void Draw(FreeCamera* _camera);

	ObjectData* GetData(){ return m_data; }

	Texture* GetTexture(){ return m_texture; }
	void SetTexture(Texture* _texture){ m_texture = _texture; }

	Texture* GetOverlay(){ return m_textureOverlay; }
	void SetOverlay(Texture* _texture){ m_textureOverlay = _texture; }

	Light* GetLight() { return m_light; }
	void SetLight(Light* _light){ m_light = _light; }

	ShaderData* GetShaderData() { return m_shaderData; }

	TwBar* InitializeTweak(GameState* _gameState);

private:

	ObjectData* m_data;
	ShaderData* m_shaderData;
	Shader* m_shader;
	Texture* m_texture;
	Texture* m_textureOverlay;
	Light* m_light;
};


#endif