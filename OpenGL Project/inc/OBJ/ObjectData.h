#ifndef OBJECT_DATA_H
#define OBJECT_DATA_H

#include <vector>
#include "ObjectLoader.h"
#include "ANT\AntTweakBar.h"

struct ObjectData
{
	TwBar* tweakBar;
	unsigned int m_indexCount, m_objectShapeSize;
	std::vector<shape_t, std::allocator<shape_t>> shapes;
	std::vector<material_t, std::allocator<material_t>> materials;
};

#endif