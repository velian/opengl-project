#ifndef LIGHT_H
#define LIGHT_H

#include "glm\glm.hpp"

class Light
{
public:

	Light(float _ambient = 0.1f, float _specular = 50.f, glm::vec3 _lightColour = glm::vec3(1, 1, 1), glm::vec3 _lightDirection = glm::vec3(45, 45, 45));

	float m_ambientLight;
	float m_specular;

	glm::vec3 m_lightColour;
	glm::vec3 m_lightDirection;

private:

	
	
};

#endif