////
// Author : James Whyte
// Description : Manages the application: initialization, updating and shutdown.
////

#ifndef APPLICATION_H
#define APPLICATION_H

#include "AntTweakBar.h"
#include "TweakBarFunctions.h"
#include "glm/glm.hpp"

struct GLFWwindow;
class GameStateManager;

class Application
{
public:

	Application();
	Application(int _screenWidth, int _screenHeight, char* _screenTitle = "GLFW Window");

	int Initialize(int _screenWidth, int _screenHeight, char* _screenTitle = "GLFW Window");

	void Run();	
	void Shutdown();

private:

	int InitializeTweakBar(unsigned int _screenWidth, unsigned int _screenHeight);

	void UpdateDelta();	

	GLFWwindow* m_window;

	GameStateManager* m_gameStateManager;

	TwBar* m_tweakBar; // Used for debugging - GUI
	glm::vec4 m_clearColour;

	//TweakBar functions
	TweakBarFunctions* m_tweakFunctions;

	bool m_running;
	float m_deltaTime;
	float m_previousTime;
	float m_currentTime;
};

#endif