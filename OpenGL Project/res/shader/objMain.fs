#version 410
in vec3 vPosition;
in vec2 vUV;
in vec3 vNormal; 
in vec3 vTangent; 
in vec3 vBitTangent; 
out vec4 FragColor; 
uniform float SpecPow; 
uniform float AmbientLight; 
uniform sampler2D diffuse; 
uniform sampler2D overlay; 
uniform vec3 CameraPos; 
uniform vec3 LightColour; 
uniform vec3 LightDir; 
void main()
{
	mat3 TBN = mat3(normalize(vTangent), normalize(vBitTangent), normalize(vNormal)); 
	vec3 N = texture(overlay, vUV).xyz * 2 - 1; 
	float d = max(0, dot(TBN * N, LightDir)); 
	vec3 E = normalize(CameraPos - vPosition.xyz); 
	vec3 R = reflect( -LightDir, vNormal.xyz); 
	float s = max (AmbientLight, dot(E, R)); 
	s = pow(s, SpecPow); 
	FragColor = texture(diffuse, vUV) * 
	vec4(LightColour * d + LightColour * s, 1) * texture(overlay, vUV);
}