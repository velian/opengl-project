#version 410

in vec2 vTexcoord;

out vec4 FragColor;

uniform sampler2D diffuse;

vec4 BoxBlur()
{
	vec2 texel = 1.0f / textureSize(diffuse, 0).xy;

	vec4 colour = texture(diffuse, vTexcoord);
	colour += texture(diffuse, vTexcoord + vec2(-texel.x, texel.y));
	colour += texture(diffuse, vTexcoord + vec2(-texel.x, 0));
	colour += texture(diffuse, vTexcoord + vec2(-texel.x, -texel.y));
	colour += texture(diffuse, vTexcoord + vec2(0, texel.y));
	colour += texture(diffuse, vTexcoord + vec2(0, -texel.y));
	colour += texture(diffuse, vTexcoord + vec2(texel.x, texel.y));
	colour += texture(diffuse, vTexcoord + vec2(texel.x, 0));
	colour += texture(diffuse, vTexcoord + vec2(texel.x, -texel.y));

	return colour / 9;
}


void main()
{
	FragColor = BoxBlur(); //BoxBlur();


	if(FragColor.a < 0.5)
	{
		discard;
	}
}