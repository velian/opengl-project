#version 410

layout(location=0) in vec3 Position; 
layout(location=1) in vec3 Normal; 
layout(location=2) in vec2 UV; 
layout(location=3) in vec3 Tangent; 

out vec3 vPosition; 
out vec2 vUV; 
out vec3 vNormal; 
out vec3 vTangent; 
out vec3 vBitTangent; 

uniform mat4 ProjectionView; 

void main()
{
	gl_Position = ProjectionView * vec4(Position, 1);
	vPosition = Position; 
	vUV = UV;
	vNormal = Normal.xyz;
	vTangent = Tangent.xyz; 
	vBitTangent = cross(vNormal, vTangent); 
}