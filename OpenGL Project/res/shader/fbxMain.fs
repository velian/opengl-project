#version 410

in vec4 vNormal; 
in vec2 vTexcoord; 
in vec4 vPosition; 

out vec4 FragColor; 

uniform sampler2D diffuse; 
uniform float AmbientLight; 
uniform vec3 LightDir; 
uniform vec3 LightColour; 
uniform vec3 CameraPos; 
uniform float SpecPow; 

void main()
{
	float d = max(0, dot(normalize(vNormal.xyz), LightDir)); 
	vec3 E = normalize(CameraPos - vPosition.xyz); 
	vec3 R = reflect( -LightDir, vNormal.xyz); 
	float s = max (AmbientLight, dot(E, R)); 
	s = pow(s, SpecPow); 
	FragColor = texture(diffuse, vTexcoord) * vec4(LightColour * d + LightColour * s, 1);
}